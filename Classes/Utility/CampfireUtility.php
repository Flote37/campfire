<?php
/**
 * Created by PhpStorm.
 * User: Florian Montalbano
 * Date: 30/09/2018
 * Time: 00:04
 */
namespace Fmontalbano\Campfire\Utility;

use TYPO3\CMS\Core\Utility\GeneralUtility;

class CampfireUtility
{
    public static function checkIfImageExists(string $imageName){
        $filePath = GeneralUtility::getFileAbsFileName("EXT:campfire/Resources/Public/Images/" . $imageName);
        return file_exists($filePath);
    }
}